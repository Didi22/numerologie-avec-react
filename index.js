import React from "react";
import ReactDom from "react-dom";
import "./style.css";

//Données du formulaire
var prenom;
var nom;
var date;
var choixAnnee;

//Chiffres
var actif;
var hereditaire;
var expression;
var intime;
var realisation;
var initial;
var evolution;
var karma;
var vie;
var climat;

//Tableau des correspondances entre les lettres et les chiffres
var correspondance = new Array();
correspondance["A"] = 1;
correspondance["B"] = 2;
correspondance["C"] = 3;
correspondance["D"] = 4;
correspondance["E"] = 5;
correspondance["É"] = 5;
correspondance["F"] = 6;
correspondance["G"] = 7;
correspondance["H"] = 8;
correspondance["I"] = 9;
correspondance["J"] = 1;
correspondance["K"] = 2;
correspondance["L"] = 3;
correspondance["M"] = 4;
correspondance["N"] = 5;
correspondance["O"] = 6;
correspondance["P"] = 7;
correspondance["Q"] = 8;
correspondance["R"] = 9;
correspondance["S"] = 1;
correspondance["T"] = 2;
correspondance["U"] = 3;
correspondance["V"] = 4;
correspondance["W"] = 5;
correspondance["X"] = 6;
correspondance["Y"] = 7;
correspondance["Z"] = 8;

//fonction de réduction de nombre
function reductionNombre(nombre){
	while(nombre >=10){
		var modulo = nombre%10;
		nombre = Math.floor(nombre /10);
		nombre += modulo;
	}
	return nombre;
}

//fonction de conversion lettre en chiffre
function conversionMotEnChiffre(lettre){
	var message = lettre.toUpperCase();
	var chiffre = 0;
	for(var i=0; i<message.length; i++){
		chiffre += correspondance[message.charAt(i)];
	}
	var res = reductionNombre(chiffre);
	return res;
}

//Nombre actif : addition des valeurs numeriques des letttres du prenom
function nombreActif(prenom){
		var res = conversionMotEnChiffre(prenom);
		return res;
}

//Nombre hereditaire : addition des valeurs numeriques des letttres du nom de famille
function nombreHereditaire(nom){
	var res = conversionMotEnChiffre(nom);
	return res;
}

//Nombre d'expression : addition des valeurs numeriques du prenom et du nom de famille
function nombreExpression(prenom, nom){
	var res1 = conversionMotEnChiffre(prenom);
	var res2 = conversionMotEnChiffre(nom);
	var res = res1 + res2;
	var resultat = reductionNombre(res);
	return resultat;
}

//Nombre intime : addition des valeurs numeriques des voyelles du prenom et du nom de famille
function nombreIntime(prenom, nom){
	var message2 = prenom.toUpperCase();
	var message1 = nom.toUpperCase();
	var info = message2 + message1;
	var resultat = 0;
	for(var i=0; i<info.length; i++){
		if(info.charAt(i) == "A" || info.charAt(i) == "E" || info.charAt(i) == "I" || info.charAt(i) == "O" || info.charAt(i) == "U" || info.charAt(i) == "Y"){
			resultat += correspondance[info.charAt(i)];
		}else{
			resultat +=0;
		}
	}
	var res = reductionNombre(resultat);
	return res;
}

//Nombre de realisation : addition des valeurs numeriques des consonnes du prenom et du nom de famille
function nombreRealisation(prenom, nom){
	var message2 = prenom.toUpperCase();
	var message1 = nom.toUpperCase();
	var info = message2 + message1;
	var resultat = 0;
	for(var i=0; i<info.length; i++){
		if(info.charAt(i) != "A" && info.charAt(i) != "E" && info.charAt(i) != "I" && info.charAt(i) != "O" && info.charAt(i) != "U" && info.charAt(i) != "Y"){
			resultat += correspondance[info.charAt(i)];
		} else if (info.charAt(i) == "A" || info.charAt(i) == "E" || info.charAt(i) == "I" || info.charAt(i) == "O" || info.charAt(i) == "U" || info.charAt(i) == "Y"){
			resultat += 0;
		}
	}
	var res = reductionNombre(resultat);
	return res;
}

//Nombre issu des initiales : addition des valeurs numeriques des initiales
function nombreInitiales(prenom, nom){
	var info1 = prenom.toUpperCase();
	var info2 = nom.toUpperCase();
	var res1 = correspondance[info1.charAt(0)];
	var res2 = correspondance[info2.charAt(0)];
	var res = res1 + res2;
	var resultat = reductionNombre(res);
	return resultat;
}

//Nombre d'evolution : addition du jour et du mois de naissance par réduction
function nombreEvolution(date){
	var donnee = date.split("/");
	var jour = donnee[0];
	var mois = donnee[1];
	var add = parseInt(jour) + parseInt(mois);
	var res = reductionNombre(add);
	return res;
}

//Table d'inclusion ou table karmique : nombres de fois ou les nombres apparaissent et lesquels sont absents
function tableKarmique(prenom, nom){
	var message1 = prenom.toUpperCase();
	var message2 = nom.toUpperCase();
	var message = message1 + message2;
	var chiffre = "";
	var cpt1 = 0, cpt2 = 0, cpt3 = 0, cpt4 = 0, cpt5 = 0, cpt6 = 0, cpt7 = 0, cpt8 = 0, cpt9 =0;
	for(var i=0; i<message.length; i++){
		chiffre += correspondance[message.charAt(i)];
	}
	for(var i=0; i<message.length; i++){
		if (correspondance[message.charAt(i)] == 1){
			cpt1++;
		} else if(correspondance[message.charAt(i)] == 2){
			cpt2++;
		}else if(correspondance[message.charAt(i)] == 3){
			cpt3++;
		}else if(correspondance[message.charAt(i)] == 4){
			cpt4++;
		}else if(correspondance[message.charAt(i)] == 5){
			cpt5++;
		}else if(correspondance[message.charAt(i)] == 6){
			cpt6++;
		}else if(correspondance[message.charAt(i)] == 7){
			cpt7++;
		}else if(correspondance[message.charAt(i)] == 8){
			cpt8++;
		}else if(correspondance[message.charAt(i)] == 9){
			cpt9++;
		}
	}
	var res = "Le 1 apparaît : " + cpt1 + " fois | Le 2 apparaît : " + cpt2 + " fois | Le 3 apparaît : " + cpt3 + " fois | Le 4 apparaît : " + cpt4 + " fois | Le 5 apparaît : " + cpt5 + " fois | Le 6 apparaît : " + cpt6 +" fois | Le 7 apparaît : " + cpt7 +" fois | Le 8 apparaît : " + cpt8 +" fois | Le 9 apparaît : " + cpt9 + " fois";
	return res;
}

//Chemin de vie : addition du jour, du mois et de l'annee de naissance
function cheminDeVie(date){
	var donnee = date.split("/");
	var jour = donnee[0];
	var mois = donnee[1];
	var annee = donnee[2];
	var add = parseInt(jour) + parseInt(mois) + parseInt(annee);
	var res = add;
	var resultat = reductionNombre(res);
	return resultat;
}

//Calcul de l'annee personnelle : addition des valeurs numeriques du jour et du mois de naissance avec annee actuelle
function calculAnneePersonnelle(date, choixDate){
	var donnee = date.split("/");
	var jour = donnee[0];
	var mois = donnee[1];
	var annee = choixAnnee;
	var add = parseInt(jour) + parseInt(mois) + parseInt(annee);
	var res = add;
	var resultat = reductionNombre(res);
	return resultat;
}

//Bonne ou mauvaise annee?
function climatAnnee(prenom, nom){
	var anneePerso = calculAnneePersonnelle(date);
	var message1 = prenom.toUpperCase();
	var message2 = nom.toUpperCase();
	var message = message1 + message2;
	var rep = "";
	var chiffre = "";
	var cpt = 0;
	for(var i=0; i<message.length; i++){
		chiffre += correspondance[message.charAt(i)];	
	}
	for(var i=0; i<message.length; i++){
		if(anneePerso == chiffre.charAt(i)){
			cpt++;
		}
	}
	if(cpt != 0){
		rep = "C'est une bonne année pour vous. Elle n'est pas une année karmique.";
	} else {
		rep = "Vous êtes dans une année karmique, vous serez amenée à surmonter des épreuves";
	}
	return rep;
}

const data =
{
    "results":[
        {
            "category": "actif",
            "number": "1",
            "interpretation": "Il est franc et direct. Il ne s'encombre pas de superflu et ne se disperse pas. Il ne baisse jamais les bras afin d'atteindre son but. Il est imposant et avec un fort caractère. Il ne craint pas les épreuves et les challenges. Ses qualités sont la témérité, le volontarisme, la détermination et le courage. C'est un leader avec une autorité naturelle. Il est passionné et de nature indépendante voire solitaire. Ne supporte pas l'autorité. Il est très protecteur en amour, toujours bien entouré et respecté par ses amis. Personne intègre, sincère et fidèle."
        },
        {
            "category": "actif",
            "number": "2",
            "interpretation": "Il est émotif et sensible. Très dépendant aux autres. Il est en apparence calme, tranquille et serein. Détestant les conflits, il cherchera toujours un terrain d'entente et d'assainir les tensions. Ses atouts se révèlent en équipe. Il manque de confiance en lui et a donc besoin de soutien pour avancer. Il cherche à aider, soutenir, conseiller. Il est empathique, très sensible et à l'écoute des autres. Il a un besoin de se sentir aimer. Il n'a pas le sens de l'initiative, et les prises de décision sont difficiles. A un besoin de reconnaissance et d'affection. Il est humble, il sait s'adapter, avec une grande compréhension des autres. En amour, il est dévoué, très prevenant et généreux."
        },
        {
            "category": "actif",
            "number": "3",
            "interpretation": "Etre très relationnel, et communicant hors pair. Très cérébral, intuitif et fin d'esprit. Aime les rencontres. Très joyeux, enthousiaste et optimiste. IL attire les regards. Il est avide d'expérience et très curieux mais a du mal à se poser et à s'attacher. Ses relations restent superficielles. Il aime voyager et n'est pas matérialiste. Il est cummuniquant, de nature fextive, sympathique, charmeur et séducteur. La parole est sa force, il sait convaincre. En amour, il est romantique mais ses relations restent éphémères."
        },
        {
            "category": "actif",
            "number": "4",
            "interpretation": "Cartésien, méthodique, pratique, possessif, rigoureux, persévérant et très organisé, perfectionniste, on peut compter sur lui. Il termine ses projets et le travail ne lui fait pas peur. Il anticipe et prévoit pour son avenir. Il a un don pour l'autorité. En contre-partie, il ne sait pas lâcher-prise. Il aime les sentiments authentiques et durables, surtout en amitié. En amour, l'idée d'un foyer le sécurise. Personne très matérialiste qui a besoin de confort et d'habitudes. Tout doit être plannifié, organisé et encadré. L'inattendu et l'imprévu l'effraient. En amour, il est un aimant profond, chaleureux et généreux. Fidèle, de parole et honnête avec de grandes valeurs. Sa famille et ses amis sont son socle."
        },
        {
            "category": "actif",
            "number": "5",
            "interpretation": "Amoureux de la vie, épicurien. N'aime pas la routine, recherche l'aventure. Son leitmotiv est le changement, la nouveauté, la découverte. Polyvalent et indépendant. Aime les défis et les challenges. Aventurier, téméraire, casse-cou et tête-brûlée ne connaissant que très peu la peur. Cherche à multiplier les expériences. Se sent vivant en se mettant en danger et en sortant de la routine. Etre ouvert, tolérant sachant s'adapter. Chaleureux, audacieux avec une intelligence émotionnelle. Il attire le regard et l'attention. Il est un amoureux passionné qui refuse la routine et recherche l'évasion."
        },
        {
            "category": "actif",
            "number": "6",
            "interpretation": "Etre d'engagement, s'épanouissant dans la régularité et le collectif. Aspire à une vie calme avec une quête de l'harmonie et de la sérénité. Rêve d'amour, et recherche l'amour idyllique. Il ne supporte pas les conflits, les imprévus. Il cherche la sérénité, le bonheur et l'harmonie. Exemplaire dans le monde du travail, très dévoué. Il est un être de service, très sensible et empathique. Il privilégiera son foyer à sa carrière professionnelle."
        },
        {
            "category": "actif",
            "number": "7",
            "interpretation": "Etre très indépendant, solitaire avec une vie intérieure très riche. Observateur, analytique, porté sur la réflexion et l'introspection. Se regénère dans la solitude. Le bruit et l'agitation ne lui conviennent pas et préfère l'isolement. Admiré des autres par ses connaissances. Se sent parfois incompris et est captivé par la faculté des autres à créer des relations. Souvent dans sa bulle, il aime étudier, apprendre. Mise sur la prudence et la sagesse. Côté professionnel, il travaille dur mais ne supporte pas l'autorité. Excellent négociateur. En amour, il recherche une personne partageant ses convictions et sa manière de vivre et avec beaucoup de tolérance."
        },
        {
            "category": "actif",
            "number": "8",
            "interpretation": "Etre apte à se transformer, à se dépasser et à supporter les épreuves. Indicidu très charismatique voire magnétique avec beaucoup de prestance et d'assurance. Il sait convaincre, peut être autoritaire, pragmatique et impose ses choix. Il sait se faire respecter. Excellent orateur et forcené du travail. Il est prêt à tout pour parvenir au succès et à la réussite. Pour ariver à ses fins, il peut utiliser la manipulation et écraser les autres; le tout sans état d'âme et de sang froid. Il aime le pouvoir, l'argent, la possession et le luxe. Il est très ambitieux et ne recule devant rien. Ce compétiteur dans l'âme va parfois dans l'extrême prenant des risques inconsidérés. Très mauvais perdant. Intolérant aux personnes faibles. Il est excessif et extrémiste. Pour son foyer, il aura comme image l'homme protecteur, bienveillant, celui dont toute la famille dépend."
        },
        {
            "category": "actif",
            "number": "9",
            "interpretation": "Altruiste, se souciant du bonheur des autres et du bien-être de l'humanité. Idéaliste souhaitant laisser une trace de son passage. Il fonctionne dans la communauté, dans le partage. Pacifiste, humaniste, voulant la paix dans le monde. Tolérant, à l'écoute, empathique et très généreux. Il veut se sentir utile, apporter son soutien, son savoir, son aide. IL aime voyager. Personne de savoir avec une faculté d'adaptation. Grand optimiste. Pour lui, tout est possible et il sait prendre des risques. Il est loyal et sincère auprès de ses amis. En amour, il fonctionne dans la fusion, dans la passion."
        },
        {
            "category": "héréditaire",
            "number": "1",
            "interpretation": "Le nombre héréditaire 1 a hérité d’une forte personnalité, et d’une grande confiance en lui ! Si vous possédez le 1 comme nombre héréditaire, vous êtes un être fort, courageux et ambitieux. Vous êtes un battant, un leader, un gagnant ! Vous avez un désir de réussite très prononcé, dans tous les domaines de votre vie. Votre sens des affaires est aussi aiguisé que votre besoin d’aller de l’avant ! Volontaire et déterminé et organisé, quand vous voulez quelque chose rien ne vous arrête ! Vous ne reculez devant aucun obstacle. On sollicite souvent votre intelligence et votre bon sens. Avec vous chaque problème a sa solution ! Vous avez un certain pouvoir d’attraction sur les autres. Meneur dans l’âme, vous aimez diriger et prendre les devants. De ce fait pouvez avoir du mal avec l’autorité… Conscient de votre potentiel, vous aimez être flatté et reconnu. Vous débordez d’assurance, ce qui peut vous conférer l’image d’une personne prétentieuse. En Numérologie le nombre d’hérédité 1 est celui des solitaires. Assez individualiste, vous avez besoin d’indépendance dans vos actions. Faire des concessions n’est vraiment pas dans votre nature. Toutefois si ça touche à l’affectif, vous pouvez réviser votre façon de voir les choses. Mauvais perdant vous ? Pas du tout, vous visez simplement la première place et ne supportez pas d’avoir le second rôle ! \nVos petits défauts : L’orgueil, l’impatience, l’individualisme, l’agressivité et l’égoïsme font aussi partie de vos attributs… Tout au long de votre vie vous devrez apprendre à maitriser ces petits débordements.\nAu final le nombre héréditaire 1 est un être fort, audacieux, résistant et plein d’aplomb ! Sa force réside principalement dans le fait qu’il sait ce qu’il veut dans la vie ! Aussi il se donnera toujours les moyens de réussir dans la voie qu’il aura choisi."
        },
        {
            "category": "héréditaire",
            "number": "2",
            "interpretation": "Le nombre héréditaire 2 a hérité d’une grande sensibilité, et d’un savoir vivre exemplaire ! Si vous possédez le 2 comme nombre héréditaire, vous êtes un être discret, calme, sociable, diplomate et extrêmement sensible. Modéré, vos pensées et vos actions sont toujours pleines de bon sens. Vous ne supportez pas la solitude, le contact humain est vital pour vous. Vous avez d’ailleurs un don certain pour les associations et le contact humain. Votre ultime priorité dans la vie se résume à la recherche d’un équilibre de vie. L’amour est une valeur essentielle à vos yeux, cependant vous avez tendance à donner plus que vous ne recevez. Dévoué corps et âme à ceux que vous aimez, vous avez aussi tendance à vous oublier ! Sensible et émotif, le malheur des autres vous touche au plus haut point. Votre profonde empathie et votre gentillesse vous poussent à toujours vouloir aider votre prochain. Vous renvoyez l’image d’une personne humaine, chaleureuse, aimante et rassurante. De nature plutôt discrète et réservée, vous préférez laisser les autres diriger. Vous êtes néanmoins un excellent collaborateur sur lequel on peut avoir toute confiance. Le travail d’équipe vous convient parfaitement. Vous possédez un petit coté fragile et émotif, vous pouvez facilement vous laisser submergé par vos peurs et vos angoisses. La pression et les conflits vous déstabilisent profondément. En revanche vous êtes un excellent médiateur, vous savez toujours trouver les mots justes pour apaiser les tensions. En numérologie le nombre d’hérédité 2 est celui des personnes qui aiment les traditions, tout ce qui est solide et durable.\nVos petits défauts : La prise de décisions, n’est pas votre qualité principale. Vous pouvez aussi vous montrer indécis, passif…Hypocrite, mais seulement dans le but de ne pas blesser, ou par peur de renvoyer une mauvaise image de vous.\nAu final le nombre héréditaire 2 est un être plein d’humanité. Une personne douce, tendre, délicate et affectueuse qui n’aspire qu’a deux choses : Être aimée à sa juste valeur, et distribuer de l’amour et de la chaleur à ceux qui en ont besoin."
        },
        {
            "category": "héréditaire",
            "number": "3",
            "interpretation": "Le nombre héréditaire 3 a hérité d’une grande capacité d’adaptation, et d’une belle ouverture d’esprit ! Si vous possédez le 3 pour nombre héréditaire, vous êtes une personne sociable, intelligente et originale, dotée de multiples talents. Vous avez une âme d’artiste ! Vous possédez un esprit créatif et inventif, ainsi que le potentiel nécessaire pour mener à bien vos projets dans la vie. Toutefois comme tous les grands artistes, vous avez besoin d’être canalisé et supervisé par des personnes capables de rationaliser votre talent. Vous êtes perçu comme un bon vivant, drôle et festif ! Un véritable bout-en-train qui aime l’aventure, la nouveauté, l’art sous toutes ses formes, et qui ose prendre des risques ! Extraverti et charmeur, vous avez un sacré bagout ! Vous savez mieux que personne comment séduire les foules Vous détestez la solitude, c’est pourquoi vous êtes généralement entouré d’un staff en permanence. En numérologie le nombre d’hérédité 3 est celui des personnes douées pour l’apprentissage et la communication. \nVos petits défauts : Gare à la superficialité, la désinvolture, la dispersion.\nAu final on peut dire du nombre héréditaire 3, qu’il est un créateur avant-gardiste capable de réaliser de grandes choses. À condition qu’il ne se laisse pas disperser par son besoin constant de nouveauté."
        },
        {
            "category": "héréditaire",
            "number": "4",
            "interpretation": "Le nombre héréditaire 4 a hérité d’un sens inégalé de l’organisation, de l’ordre et de la rigueur. Si vous possédez le 4 pour nombre héréditaire, vous êtes une personne équilibrée, fiable, sérieuse et droite. Vous avez un grand sens du devoir et des responsabilités. Cependant votre rigueur et votre sérieux ne laissent que très peu de place à l’amusement et à la légèreté. Vous avez du mal à sortir de votre zone de confort, dans votre vie tout est structuré, rangé et planifié. Pragmatique et méthodique, vous possédez une capacité de travail impressionnante. Vous avez toutes les cartes en main pour vous assurer une carrière prometteuse, ainsi qu’une vie stable et confortable. En dépit de votre rigueur, vous pouvez avoir du mal à faire des choses qui ne vous passionnent pas. Ce désintérêt se traduira alors par une certaine passivité. Quoi qu’il en soit, vous êtes un être solide, courageux, vaillant et très actif. Votre entourage voit en vous le travailleur acharné que vous êtes, mais ne perçoit pas toujours votre fragilité intérieure ! Pudique dans vos sentiments, vous vous cachez derrière une froideur apparente. Néanmoins lorsque vous êtes en confiance, vous êtes capable donner beaucoup de votre personne. En numérologie, le nombre d’hérédité 4 est celui des personnes sérieuses et solides, celui des piliers.\nVos petits défauts : Vous pouvez manquer de fantaisie, de tolérance et d’ouverture d’esprit. Votre amour de l’ordre et à discipline frôle parfois la maniaquerie… Ce qui à la longue peut exaspérer votre entourage.\n Au final on peut dire du nombre héréditaire 4 qu’il est un bourreau de travail, capable d’analyser et de comprendre toutes sortes d’informations. Le nombre héréditaire 4 vit son présent pour mieux construire son avenir !"
        },
        {
            "category": "héréditaire",
            "number": "5",
            "interpretation": "Le nombre héréditaire 5 a hérité d’un tempérament libre, curieux, combatif et ardent ! Le nombre d’hérédité 5 est un précurseur à l’esprit large, qui déborde d’idées et de projets. Tout ce qu’il accomplit dans la vie, se fait avec ardeur et persévérance. Il ne tolère pas l’échec, et encore moins le fait de s’apitoyer sur son sort ! Si vous possédez le 5 pour nombre héréditaire, vous êtes une personne ingénieuse, dynamique, enthousiaste et assez indépendante. Vous possédez aussi de grandes qualités humaines qui font de vous un être apprécié et respecté. Authentique et généreux, vous êtes capable de vous adapter à toutes les situations et à tous les milieux sociaux. Toujours en activité, vous avez soif d’aventures, de voyages, de nouveauté et de découvertes. Les défis vous stimulent au plus haut point ! Téméraire, vous pouvez vous lancer à l’assaut de choses dont vous ne mesurez pas toujours les dangers. Votre curiosité et votre spontanéité peuvent toutefois vous amener à commettre des actes irréfléchis. Quoi qu’il en soit, en numérologie, le nombre d’hérédité 5 est celui de la persévérance, celui des personnes qui vont toujours de l’avant ! \nVos petits défauts : Gare à la dispersion, l’instabilité, la vantardise, et le libertinage.\n Au final on peut dire du nombre héréditaire 5 qu’il est doté d’un esprit vif et intelligent. C’est un combattant qui possède énormément de bravoure et de courage !"
        },
        {
            "category": "héréditaire",
            "number": "6",
            "interpretation": "Le nombre héréditaire 6 a hérité d’un profond sens des responsabilités, de le justice et de l’honnêteté ! Si vous possédez le 6 pour nombre héréditaire, vous êtes serviable, loyal et plein de bonté. Droit, honnête et fidèle, vous êtes quelqu’un sur qui on peut toujours compter. Généreux, humain et chaleureux, vous inspirez la confiance et la sympathie. À vos cotés, les autres se sentent immédiatement rassurés. Vous aimez la sérénité, l’équilibre et les rapports calmes et harmonieux. En revanche l’injustice vous révolte au plus haut point ! La famille occupe une place importante dans votre existence. Vous êtes un ami en or, un partenaire fidèle, un parent exemplaire, un frère de sang, de coeur… Votre sens des responsabilités est exemplaire, c’est d’ailleurs avec grand plaisir que vous accomplissez vos taches et vos obligations. Vous êtes un pilier, un chef de meute, un ange-gardien ! Capable de protéger les vôtres, et d’assurer leur avenir mieux que personne. En numérologie le nombre d’hérédité 6 est celui des personnes profondément humaines, celles qui luttent pour de nobles causes. Il fait référence à une immense gentillesse. Cependant l’intérêt que vous portez à autrui, fait que vous avez tendance à vous oublier ! Vous devrez tout au long de votre vie être capable de prendre soin de vous, comme vous prenez soin des autres. \nVos petits défauts : Gare à l’indécision, l’instabilité, l’intrusion, la jalousie et la rancune dans votre vie personnelle. Votre grande affectivité peut parfois être étouffante. Hypersensible et émotif, vous pouvez avoir recours à l’ironie pour masquer vos joies… où vos peines.\nAu final on peut dire du nombre héréditaire 6, qu’il est un grand sensible doté d’un coeur en or !"
        },
        {
            "category": "héréditaire",
            "number": "7",
            "interpretation": "Le nombre héréditaire 7 a hérité d’un tempérament calme et posé, et d’une profonde sagesse ! Si vous possédez le 7 pour nombre héréditaire, vous êtes une personne tolérante, sensée et extrêmement réfléchie. Vous possédez une intelligence supérieure à la moyenne, et une grande ouverture d’esprit. Le rôle de psychologue vous sied à merveille, vous êtes toujours de bons conseils pour les autres. La justice et la vérité sont des valeurs essentielles à votre existence. Méthodique et perfectionniste, vous vous investissez corps et âme dans tout ce que vous faites. Vous avez une capacité impressionnante d’analyse et de réflexion sur le monde. Capable de passer des heures à apprendre, à réfléchir, votre vie intérieure est généralement riche et intense. Pas toujours sociable, vous pouvez être catalogué de personne froide et distante. Il n’est pas rare de vous voir être attiré par la spiritualité, l’ésotérisme, la religion…Vous n’êtes que peu concerné par le coté matériel de la vie. En numérologie le nombre d’hérédité 7 est celui des personnes solitaires. Celles qui ont besoin de calme pour méditer, s’organiser, se ressourcer… En dépit de votre grande sagesse et votre raison, vous êtes aussi un inquiet. Vous pouvez facilement vous laisser aller à des périodes de doutes, d’angoisses, de mélancolie, et vous replier sur vous-même. \nVos petits défauts : Gare à votre coté ermite, il vous éloigne des autres.\n Au final on peut dire du nombre héréditaire 7 qu’il est un grand sage, doté d’une intelligence et d’une bonté d’âme extraordinaire !"
        },
        {
            "category": "héréditaire",
            "number": "8",
            "interpretation": "Le nombre héréditaire 8 a hérité d’une nature courageuse et d’une détermination à toute épreuve ! Si vous possédez le 8 pour nombre héréditaire, vous êtes une personne ambitieuse et audacieuse. Vous savez exactement ce que vous désirez dans la vie. Réussir matériellement est votre priorité, vous désirez gagner votre indépendance et vous assurer un avenir prospère. Forcené de travail, vous ne reculez devant rien pour mener à bien vos projets. Vous êtes une personne d’action, rien ne vous fait peur, l’audace, la persévérance et la détermination accompagnent toutes vos démarches. Doué pour les affaires, excellent négociateur, et compétiteur hors pair, vous ne faites pas dans la dentelle quand il s’agit d’imposer vos idées. Plus meneur que suiveur, vous êtes fait pour diriger. Vous aimez avoir le sentiment de contrôler les choses. Toutefois vous êtes droit dans vos bottes, vous possédez de grandes valeurs morales et un sens aigu de la justice. Ce qui vous permet de gagner la confiance et le respect des autres. Impulsif et autoritaire, vous pouvez aussi devenir écrasant et faire preuve d’intolérance à l’égard de votre entourage . \nVos petits défauts : Gare à l’obstination, l’intransigeance, l’intolérance, l’autorité et l’agressivité.\n Au final on peut dire du nombre héréditaire 8 qu’il est un vaillant conquérant !"
        },
        {
            "category": "héréditaire",
            "number": "9",
            "interpretation": "Le nombre héréditaire 9 a hérité des siens dignité, fierté et sens de l’honneur ! Si vous possédez le 9 pour nombre héréditaire, vous êtes une personne noble, généreuse et altruiste totalement impliquée dans le bien-être des autres. Vous avez un sens inné de la justice et de la dignité. Vous êtes un humaniste dans l’âme, un être emphatique totalement désintéressé par le coté matériel de la vie. Vous ne supportez ni la misère, ni et les inégalités qui règne dans ce monde. La société actuelle ne vous convient pas toujours, vous la jugez bien trop individualiste. D’ou votre gout prononcé pour les voyages, les causes humanitaires, les rencontres, les différences de cultures… Extrêmement créatif, vous débordez d’idées et de talent. Vous êtes capables de réaliser de très grandes choses. Bien que vous soyez conscient et fier de vos capacités, vous savez rester humble et ne vous mettez jamais en avant.\nVos petits défauts : Vous devez apprendre à gérer votre susceptibilité et votre émotivité.\n Au final on peut dire du nombre héréditaire 9, qu’il est un fervent défenseur de l’amour universel !"
        },
        {
            "category": "expression",
            "number": "1",
            "interpretation": "LE 1 EST LE NOMBRE DE LA PERSONNALITÉ ET DE L’INDÉPENDANCE\n Vous êtes une personne volontaire, indépendante, ambitieuse, énergique et autoritaire : individualiste, vous êtes née pour commander. Peu importe votre spécialité, vous avez confiance en vous et vous mettrez tout votre charme et votre détermination pour accéder aux postes de commande. Pour vous, la réussite réside dans l'efficacité au travail, l'acharnement et l'excellence, mais vous êtes parfois des personnes tellement exigeantes envers votre entourage qu’il devient difficile de vous côtoyer. Votre ambition pourrait vous rendre égoïste, dominateur et tyrannique. Vous êtes entêté et buté, et il est donc très difficile de vous conseiller. Les relations sociales ont une grande importance car elles vous permettent de connaître des gens influents, mais surtout de vous faire connaître d’eux. On vous retrouvera donc le plus souvent en compagnie de gens qui ont fait leurs preuves, que ce soit dans le même champ d'activité que vous ou dans un domaine connexe.\nAMOUR : En amour, le nombre 1 reflète encore son autorité dans votre vie à deux. Votre partenaire doit être docile et fidèle, mais également vous soutenir, vous seconder et …vous admirer. Sous votre façade froide et dure , vous êtes une personne sentimentale, généreuse et passionnée, qui aspire à un idéal et à un amour durable. \nSANTE : Au niveau de votre santé, vos points faibles sont le cœur, la circulation sanguine et les yeux.\nARGENT : côté argent, vous avez beaucoup de chance que vous risquez toutefois de gâcher à cause de votre caractère. En effet, vous pouvez gagner facilement une fortune et la perdre la minute qui suit."
        },
        {
            "category": "expression",
            "number": "2",
            "interpretation": "LE 2 EST LE NOMBRE DE L’ÉQUILIBRE ET DE LA BONTÉ\nVous êtes une personne équilibrée, diplomate et pondérée. Sensible et émotive, vous paraissez calme, réservée et tranquille, mais vous êtes souvent tendue. Vous avez plus de facilité à agir en équipe qu' individuellement. Vous avez besoin d'être régies par une forte hiérarchie et vous trouverez votre place dans celle-ci. Contrairement aux types 1 qui sont des leaders, vous êtes des exécutants et vous ne vous sentez pas le moins du monde infériorisés dans cette position. Vous êtes une personne douée pour les contacts et les négociations, ce qui fait de vous d’excellents collaborateurs. Mais pour de bons résultats, vous avez besoin d’un environnement serein et harmonieux, sans querelle ni dispute. Pour avancer dans la vie, vous devez compter sur les autres, que ce soient les collègues de travail ou la personne qui partage votre vie, car vous avez de la difficulté à prendre vous-même les décisions. Les autres recherchent et apprécient votre compagnie pour votre charme et la gentillesse que vous répandez avec générosité autour de vous, mais aussi parce que vous savez les écouter, les comprendre et les réconforter.\nAMOUR : En amour, vous êtes le conjoint parfait et vous est prêt à toutes les concessions pour faire durer votre union.De nature douce, chaleureuse et affectueuse, vous avez un immense besoin de tendresse et d'amour. Vous unirez votre destinée avec un être aimé sans nécessairement être sûres de vos sentiments. Puisque vous avez une tendance marquée pour la rêverie et l'utopie, vous aurez besoin d’un partenaire qui sache vous ramener à la réalité.\nSANTE : Au niveau de votre santé, vos points faibles sont les glandes et l'estomac.\nARGENT : Côté argent, vous éviterez les placements hasardeux car vous n’aimez pas le risque. Vous savez économiser, et de cette façon, vous voyez vos gains augmenter régulièrement. Cependant, votre bon cœur pourrait vous amener à dépenser pour les autres ou encore à prêter à de faux amis. Méfiez-vous."
        },
        {
            "category": "expression",
            "number": "3",
            "interpretation": "LE 3 EST LE NOMBRE DE L’EXPRESSION ET DES DONS ARTISTIQUES\nVous pouvez compter sur une imagination débordante et un optimisme inné pour ne pas chercher à vous compliquer la vie, même quand la situation paraît dramatique. Vous êtes une personne expressive, créative, amicale et généreuse, et vous communiquez votre enthousiasme aux autres. Votre ambition vous amène à savoir profiter de toutes les occasions qui s'offrent à vous, que ce soit dans le domaine social ou professionnel. Doté d’un bon sens pratique et possédant de nombreux talents, vous pouvez réussir dans différents domaines car de plus, vous avez une grande capacité à vous adapter à de nouveaux milieux. D’un autre côté, votre principal défaut est la tendance à vous disperser :vous entreprenez plein de choses que vous ne finissez pas. Sociable, vous aimez partager vos joies et votre bonheur avec votre entourage, en particulier dans un contexte familial. Votre chez-soi représente beaucoup pour vous, vous le voulez chaleureux, invitant et portant à la détente.\nAMOUR : En amour, vous êtes une personne gaie et joviale, capable d’un amour profond et passionné.Cependant, vous prenez beaucoup de temps avant de vous engager dans une union sérieuse, ce qui vous fait parfois passer pour quelqu’un de volage. Vous aimez paraître, être aimé ,vous avez besoin d’un grand public et vous vous entourez de beaucoup d’amis, mais ces amitiés sont souvent superficielles.\nSANTÉ : Au niveau de votre santé, vos points faibles sont le foie et la circulation sanguine.\nARGENT : Côté argent, vous attirez la chance financière et même si vous n’accédez jamais à la fortune, vous réussissez tout de même à bien vivre. Il faut dire que vous êtes généreux et que vous dispersez assez facilement vos biens."
        },
        {
            "category": "expression",
            "number": "4",
            "interpretation": "LE 4 EST LE NOMBRE DE LA RÉALISATION ET DU SENS PRATIQUE\nVous êtes une personne qui a un grand sens de l’organisation et de la précision. Vous recherchez la rigueur dans l'exécution des tâches qui vous sont confiées et vous réalisez tout avec discipline et acharnement. On vous fait confiance pour vos qualités d’ordre et de méthode et pour l’ardeur que vous mettez à accomplir votre travail. Cependant, il faut que les choses soient claires : à chacun sa tâche et que chacun fasse sa part de travail avec la même rigueur que celle dont vous faites montre. Si vous êtes un employé exemplaire, vous seriez tyrannique comme employeur, car vous trouvez que les autres n’en font jamais assez. Vous êtes même souvent intransigeants. De nature ,vous êtes une personne patiente, stable et persévérante qui aime le calme et la discrétion. Vous avez rarement le sens de l’humour. Vous manquez souvent de tact car vous êtes franc et direct, droit et honnête. Vous êtes entêté et capable de tout pour obtenir ce que vous désirez. Loin d'être des personnes créatives, vous vous attachez à ce qu'il y a de plus conservateur dans la vie et vous êtes très respectueux du passé. Vous n’aimez pas vous entourer d'une vaste cour. Seules importent les personnes avec lesquelles vous êtes en relation professionnelle ou familiale et vous êtes prêtes à tout pour créer une ambiance sécuritaire, leur montrant ainsi votre générosité et votre fidélité.\nSANTE : Au niveau de votre santé, vos points faibles sont les os, les dents et les articulations.\nAMOUR : En amour, vous êtes capable de sentiments profonds et stables mais vous êtes une personne peu démonstrative. Vous êtes une personne franche et loyale, discrète, voire même réservée et vous détestez les commérages, et pour toutes ces qualités, vos amis vous sont fidèles. La discrétion, voire la réserve, sont vos caractéristiques.\nARGENT : Côté argent, vous êtes économe et la prudence vous oblige à ne jamais prendre de risque avec votre argent. Vous aimez l’argent surtout pour la sécurité qu’il vous procure, mais vous savez profiter des bonnes occasions."
        },
        {
            "category": "expression",
            "number": "5",
            "interpretation": "LE 5 EST LE NOMBRE DE LA VIE ET DE LA VERSATILITÉ\n Le nombre 5 fait de vous des personnes dynamiques et éprises de liberté qui se sentent prises en souricière entre les expériences du passé et un besoin de découvrir un avenir chargé de nouveauté.Le 5 s'intéresse à des domaines variés et il n'est pas rare de le voir changer de profession. Les voyages lui sont favorables. Il a besoin de bouger et ses qualités d'adaptation ainsi que sa facilité de contact le font souvent réussir. Cette facilité d'adaptation vous permet également de vous sortir de toutes les situations. Vous préférez le travail individuel et vous partagez difficilement avec les autres vos idées et vos découvertes. Vous savez faire preuve d'une grande souplesse d'esprit qui vous permet de vous inscrire dans un groupe où vous deviendrez rapidement des leaders. Puisque vous agissez en suivant vos impulsions, votre entourage vous trouve instable. Étant une personne impatiente, avec une certaine propension à la dépression, vous avez souvent besoin d'évasion pour vous refaire une santé mentale et permettre à votre créativité d'être à l'affût de la nouveauté.\nAMOUR : En amour, votre conjoint devra être prêt à s'adapter facilement, à être souple et autonome, car ce sont les qualités que vous recherchez chez les autres. On ne peut pas dire que vous soyez une personne sentimentale, mais vous savez plaire et séduire par votre esprit, vos belles paroles et votre délicatesse. Bien que votre goût du changement ne soit pas toujours compatible avec les unions durables, il arrive que vous vous unissiez définitivement pour la bonne cause. Si vous êtes compris, vous serez le partenaire idéal.\nSANTÉ : Au niveau de la santé, vos points faibles sont le système nerveux, les poumons et la gorge.\nARGENT : Côté argent, vous êtes généreux quel que soit l’état de vos finances. Vous aimez l’argent, mais plus pour le plaisir qu’il vous procure que pour la sécurité matérielle qu’il apporte.Vous devriez songer à faire gérer vos biens par une personne de confiance!"
        },
        {
            "category": "expression",
            "number": "6",
            "interpretation": "LE 6 EST LE NOMBRE DE L’HARMONIE ET DU SENS DES RESPONSABILITÉS\nVous êtes des personnes charmantes et harmonieuses, sensibles, remplies d’amour et de douceur. Votre humanisme vous amène beaucoup d’amis. Votre esprit conciliant vous permet de vous sentir à l’aise en situation de conflit car vous savez faire preuve de diplomatie pour les régler, à la condition toutefois que l’on vous laisse carte blanche . Vous avez le sens de la beauté et de l’esthétisme, vous aimez les belles choses, et certains 6 pourront faire des carrière artistiques. Si vous manquez parfois de rigueur et d’ambitions, vous avez en revanche un grand sens des responsabilités que vous assumez d’ailleurs pleinement. Étant des personnes instables, vous recherchez constamment de nouveaux défis, ce qui se traduit par une tendance à vouloir trop en faire. Vous aimez le travail minutieux ,précis et bien fait, mais il ne faut pas vous enfermer dans les entreprises à long terme car vous n’y seriez pas à l’aise et risqueriez d’abandonner en cours de route. D’ailleurs, vous devez souvent votre réussite aux gens qui vous entourent. Les 6 doivent faire attention à une sensualité débordante ainsi qu’à des tendances à l’instabilité et l’indécision dues à votre hypersensibilité.\nAMOUR : En amour, vous avez le souci de plaire et vous recherchez le bonheur dans les plaisirs et l’amour. Toujours en quête du bonheur, vous tombez facilement en amour, mais si c’est généralement de courte durée, c’est profond et intense. Votre équilibre général dépend souvent de votre équilibre sentimental. Quand vous trouvez le bonheur et l’équilibre dans une union, vous faites tout pour que ce soit durable. Quand vous vous unissez, c’est pour toujours et vous vous occupez de votre foyer car vous avez le sens de la famille. Vous aimez vous entourer d’un cadre de vie chaleureux et les valeurs sociales sont très importantes pour vous.\nSANTÉ : Au niveau de la santé, vos points faibles sont la gorge, la voix et les organes sexuels (surtout chez les femmes).\nARGENT : Coté argent, vous êtes assez généreux sans pour autant vous ruiner, car vous privilégiez la prudence dans vos dépenses financières. Vous êtes des bons gestionnaires et vous préférez la sécurité ."
        },
        {
            "category": "expression",
            "number": "7",
            "interpretation": "LE 7 EST LE NOMBRE DU MYSTÈRE ET DU POUVOIR D’ANALYSE\nCe qui vous caractérise surtout est votre sens de l’organisation, de l’analyse et de l’étude. Vous avez une soif de connaissance inépuisable et vous savez prendre tout le temps qu'il faut pour vous rendre à la limite de l'exploration.Cette démarche doit cependant se faire dans un climat calme d'où toute autorité aura été écartée, car vous êtes une personne indépendante qui préfère tout découvrir par elle-même. Votre personnalité de perfectionniste vous rend difficile à satisfaire et vous pousse en même temps à refuser les avis et les jugements des autres. D’un tempérament timide et réservé, vous êtes peu sociable et vous préférez travailler en petit groupe plutôt qu’avec une foule de collègues, et c’est encore mieux si vous pouvez travailler seule. Vous avez une intuition fabuleuse et vous savez réussir en dehors des sentiers battus grâce à l’originalité que vous appliquez à toutes vos entreprises. Bien que vous ayez sans cesse le désir d’entreprendre de nouvelles expériences, vous ne vous égarez pas sur des chemins compliqués et vous poursuivez sur la voie la plus simple que votre imagination aura découvert. Votre grande vie intérieure est faite d’analyse, de spiritualité et de sérénité et vous êtes toujours en quête de causes premières. Votre personnalité comporte également des risques de doutes et d’inquiétudes, de mélancolie et de replis sur vous-même , et il vous arrive d’avoir des tendances au pessimisme.\nAMOUR : En amour, votre indépendance et votre difficulté à exprimer vos sentiments ne facilitent pas l’union durable. Vous êtes quelqu’un de très exigeant avec votre partenaire qui doit respecter votre indépendance et partager les mêmes intérêts que vous . Pour vous, l’amitié est plus importante que l’amour, et, même si vous avez peu d’amis, vous êtes toujours d’une loyauté exemplaire et d’une grande générosité avec eux.\nSANTÉ : Au niveau de la santé, vos points faibles sont les nerfs et le psychisme.\nARGENT : Côté argent, vous n’aimez pas particulièrement l’argent, cependant, bien que vous soyez économe, il peut vous arriver parfois de faire une folie sur un coup de tête."
        },
        {
            "category": "expression",
            "number": "8",
            "interpretation": "LE 8 EST LE NOMBRE DE L’ORIGINALITÉ ET DES DONS D’ORGANISATION\nVous êtes des personnes énergiques et vous avez besoin d’activités qui vous permettent de dépenser votre énergie vitale. Courageuses, vous êtes représentées par un sentiment de force dans l’atteinte de vos objectifs, et ce, dans tous les domaines. Cette force est caractérisée par une résistance à toute épreuve face aux défis de la vie. Votre ténacité vous permet d’atteindre vos buts; rien ne vous arrête quand vous vous êtes lancées sur le chemin de la réussite. Vous êtes des personnes autoritaires, pragmatiques, ayant un sens du concret et un esprit entreprenant qui vous rendent aptes à prendre des décisions rapides. On note toutefois chez-vous une tendance à la dureté et à l’agressivité envers les personnes qui ne vous intéressent pas. Vous êtes loyales mais rancunières et vous avez horreur de la médiocrité :vous aimez que les choses soient claires et nettes. Intransigeantes, vous ne vous laisser ébranler par aucun compromis. Au travail, si on ne vous laisse pas exprimer votre sens créatif et vos idées, vous vous sentirez frustrée, mais comme employeur, vous exigez beaucoup des autres, autant que vous exigez de vous-même. Vous aimez la justice et la logique, le pouvoir et la réussite matérielle. Mais attention aux risques d’extrémisme, d’arrivisme, d’ambitions démesurées et de domination.\nAMOUR : En amour, votre grande faiblesse , il vous faut des conquêtes rapides, la patience n’étant pas votre point fort. Vous aimez avec passion , et bien que vous ayez parfois l’air absorbé et distant, vous n’aimez pas moins pour autant. Dans une union, le 8 domine , et avec vous, on sait ou on va…Vous êtes des personnes franches et jalouses, aussi bien en amour qu’en amitié, mais vous pouvez vous dévouer sans compter pour vos véritables amis.\nSANTÉ : Au niveau de la santé, vos points faible sont la tête et les organes sexuels (surtout chez les hommes), ainsi que des risques de blessures et d'opérations.\nARGENT : Coté argent, vous êtes douée pour les placements et faire de bons investissements. Vous êtes une personne généreuse autant pour votre famille que pour les autres et vous pouvez dépenser sans compter pour une bonne cause ou une action positive."
        },
        {
            "category": "expression",
            "number": "9",
            "interpretation": "LE 9 EST LE NOMBRE DE L’IDÉAL. IL POSSÈDE DANS SON ÉVOLUTION TOUTES LES EXPÉRIENCES DES AUTRES NOMBRES\nDe caractère passionné, vous êtes des personnes débordantes d'énergie : une tâche qui vous est confiée est réalisée vite et bien. C'est dans l'action humanitaire que vous vous sentirez le plus à l'aise, car vous êtes fondamentalement altruistes et éprises de justice, et votre dévouement est sans limite. Sensibles aux problèmes des autres, vous aimez corriger les inégalités que la vie aura créées et, pour ce faire, vous aurez souvent recours au dialogue franc. Au travail, vous êtes un soutien précieux pour ceux et celles avec qui vous devez composer et vous inspirez confiance, quelle que soit la difficulté rencontrée. Votre grande capacité d’assimilation est un atout important dans vos réussites, cependant, il faut ajouter que vous travaillez efficacement uniquement si vous y croyez. Vous avez de bonnes idées fondées sur vos expériences personnelles et vous savez les faire partager. On retrouve chez-vous une tendance à perdre de vue la réalité et votre idéalisme est parfois excessif. Vous manquez un peu de sens pratique et vous vous laissez parfois influencer trop facilement.\nAMOUR : En amour, vous êtes des personnes dignes, fidèles, tendres et prêtes à tout pour rendre l’autre heureux. Sans être ni romantiques ni sentimentales, vous faites preuve d’affection, vous êtes disponibles et ouvertes aux concessions. Émotives, vous avez besoin de vous sentir aimées et vous demandez sans cesse des preuves de cet amour. Pour vous, le mariage est sacré, mais vous devez trouver l’équilibre dans votre couple, sinon, vous risqueriez de vous laissez entrainer dans des aventures qui terniraient votre image.\nSANTÉ : Au niveau de la santé, vos points faibles sont le psychisme et le système lymphatique\nARGENT : Coté argent, la chance est l’atout principal de votre réussite financière. Puisque votre altruisme et votre générosité vous pousse parfois à vous endetter ,vous devrez surveiller vos finances ou les confier à une personne compétente."
        },
        {
            "category": "intime",
            "number": "1",
            "interpretation": "Si votre nombre intime est le 1, vous êtes avant tout quelqu'un d'indépendant, de libre et qui n'aime guère se plier aux ordres à al loi ou aux obligations diverses ! Passionné(e), parfois impulsif(ve), vous êtes une forte tête qui fonce parfois sans réfléchir ! Très ambitieux(se), vous êtes courageux, endurant et visez avant tout la réussite et les postes à commandements ou les professions d'indépendants. En amour, vous vous posez généralement tard, mais vous montrez une fois casé, passionné et dévoué à l'autre."
        },
        {
            "category": "intime",
            "number": "2",
            "interpretation": "Sensible, aimant, généreux et foncièrement gentil(le), ce nombre intime 2 vous confère une nature généralement très appréciée des autres. Vous aimez aider, soigner, et possédez un don particulier pour prendre soin des enfants mais aussi des animaux. L'amour est un de vos let-motif ! Peu matérialiste, vous chercherez avant tout dans la vie à aimer, à être aimé, mais aussi à créer, car cette vibration 2 vous apporte un beau pouvoir créatif !"
        },
        {
            "category": "intime",
            "number": "3",
            "interpretation": "Avec un nombre intime 3 on est généralement très communicatif, drôle, curieux, et cultivé ! Le nombre intime 3 adore le mouvement la nouveauté et peut se monter très léger, parfois trop, ne sachant pas vraiment se discipliner ou se limiter. Le nombre intime 3 cultive généralement de nombreuses amitiés, et peut ne pas toujours être fidèle. Il réussit généralement brillamment dans les domaines du commerce, des langues, ou encore de la communication."
        },
        {
            "category": "intime",
            "number": "4",
            "interpretation": "Avec un nombre intime 4, on trouve généralement une personne très droite, honnête sincère et très loyale ! Les nombres intimes 4 sont généralement des gens entiers, en qui l'on peut avoir toute confiance et qui sont des fanatiques de travail, d'ordre tout en cultivant le goût de l'effort ! Un nombre intime 4 est avant tout fidèle, stable, mais peut avoir parfois du mal à montrer leur affection, ou leurs sentiments, tant ils sont des êtres intérieurs, parfois timides et peu sûrs d'eux..."
        },
        {
            "category": "intime",
            "number": "5",
            "interpretation": "Les nombres intimes 5 sont généralement des personnalités hautes en couleurs, drôles, amusantes, mais aussi parfois instables ! Difficiles de suivre les nombres intimes 5 qui sont souvent désireux de nouveautés d'aventures, et de légèreté avant tout ! Aventureux, passionnés, très démonstratifs, les nombres intimes 5 aiment partir à l'aventure, et tenter de nouvelles expériences. Mais ils peuvent être manipulateurs, parfois menteurs, et pas toujours très faciles à diriger, leur caractère n'apprécient guère les ordres ou la discipline, et cela peut parfois les amener à vivre de façon irrégulière, sans se soucier vraiment des lendemains qui peuvent déchanter."
        },
        {
            "category": "intime",
            "number": "6",
            "interpretation": "Les nombres intimes 6 en voyance, sont des êtres généralement simples, gentils, proches des autres, mais aussi de la nature et des animaux. Ils vivent avant tout pour l'amour, l'affection et les sentiments. La famille, le couple, et les enfants sont avant tout leurs préoccupations majeures. Ce sont des amis généreux, attentifs, et fidèles tant en amour qu'en amitié d'ailleurs, et ils aiment avant tout la stabilité, un peu trop parfois d'ailleurs, puisqu’ils peuvent vite devenir casaniers, voir légèrement ennuyeux !"
        },
        {
            "category": "intime",
            "number": "7",
            "interpretation": "Les personnes possédant un nombre intime 7 sont généralement très intelligentes, dotées d’une excellente mémoire, elles aiment se pencher sur les problèmes méta-physiques, la spiritualité, ou les domaines de l'ésotérisme. Ce sont souvent des âmes solitaires, qui apprécient de prendre de temps en temps leur distance vis à vis des autres. Imaginatifs, rêveurs, ils peuvent parfois se perdre dans leur petit monde et avoir du mal au final à affronter les soucis quotidiens ou les problèmes matériels... Mais ils restent avant tout des êtres simples, gentils, avec certes, parfois un fort caractère, mais qui sont heureux de partager leur savoir et leurs expériences."
        },
        {
            "category": "intime",
            "number": "8",
            "interpretation": "Le 8 en numérologie laisse entrevoir une personnalité solide, forte, dotée d'un caractère parfois dur et qui se montre généralement ambitieux, et très attaché aux choses matérielles de ce monde. Ils peuvent paraître froids ou encore hautains, mais ce sont au final des êtres entiers et généreux, qui, une fois qu'on les connaît bien laissent entrevoir un cœur gros comme ça et une belle sensibilité qu’ils aiment pourtant à cacher, juste histoire de se protéger !"
        },
        {
            "category": "intime",
            "number": "9",
            "interpretation": "Le nombre intime 9 laisse entrevoir une personnalité très humaine, chaleureuse, éprise de voyages, de découvertes et très ouverte aux autres cultures et aux autres modes de pensées. Avec un nombre intime 9, on aime aider son prochain et l'humanitaire ou encore le social peut être une donne importante de la destinée. La vibration du 9 indique également beaucoup d’originalité, et d'indépendance. Avec un nombre intime 9 vous rêverez souvent d'horizons lointains, et ne sauriez vous satisfaire d'un petit train-train quotidien fade ou ennuyeux."
        },
        {
            "category": "réalisation",
            "number": "1",
            "interpretation": "Grandes compétences. Maîtrise. Indépendance. Responsabilité. Autant d’atouts qui permettent un épanouissement professionnel total."
        },
        {
            "category": "réalisation",
            "number": "2",
            "interpretation": "Excelle dans le travail en équipe. Grand sens de l’adaptation."
        },
        {
            "category": "réalisation",
            "number": "3",
            "interpretation": "Don pour les discours et les échanges. Réussite dans un travail lié aux contacts, à la communication. Responsabilités."
        },
        {
            "category": "réalisation",
            "number": "4",
            "interpretation": "Grand gestionnaire que ce soit pour un projet ou pour les ressources humaines."
        },
        {
            "category": "réalisation",
            "number": "5",
            "interpretation": "Grande capacité d’adaptation. Dynamisme. Flexibilité. Fonceur. Multiplication des expériences."
        },
        {
            "category": "réalisation",
            "number": "6",
            "interpretation": "Don pour la gestion des ressources humaines et la diplomatie. Réussite artistique et toutes professions liées à un public ou une clientèle directe."
        },
        {
            "category": "réalisation",
            "number": "7",
            "interpretation": "Réussite liée à l’imagination, dans l’originalité et de façon indépendante. Grandes facultés de compréhension et de réflexion ."
        },
        {
            "category": "réalisation",
            "number": "8",
            "interpretation": "Energie, franchise, autorité, dynamisme, persévérance, rigueur."
        },
        {
            "category": "réalisation",
            "number": "9",
            "interpretation": "Grande sensibilité, altruisme, idéalisme. Professions d’aide à la personne."
        },
        {
            "category": "issu des initiales",
            "number": "1",
            "interpretation": "le début, le principe masculin (yang), l’action, l’autorité."
        },
        {
            "category": "issu des initiales",
            "number": "2",
            "interpretation": "le principe féminin (yin), le réceptif, les alliances et la sensibilité."
        },
        {
            "category": "issu des initiales",
            "number": "3",
            "interpretation": "la création, l’enfant dans le couple, la communication."
        },
        {
            "category": "issu des initiales",
            "number": "4",
            "interpretation": "le travail, la stabilité, la rigueur."
        },
        {
            "category": "issu des initiales",
            "number": "5",
            "interpretation": "la liberté, le changement"
        },
        {
            "category": "issu des initiales",
            "number": "6",
            "interpretation": "la beauté, la recherche d’équilibre et d'harmonie."
        },
        {
            "category": "issu des initiales",
            "number": "7",
            "interpretation": "le mental, la réflexion, les études, le spirituel."
        },
        {
            "category": "issu des initiales",
            "number": "8",
            "interpretation": "l'aspect matériel, la construction et la destruction, les gains et les pertes."
        },
        {
            "category": "issu des initiales",
            "number": "9",
            "interpretation": "l'altruisme, la fin et le recommencement."
        },
        {
            "category": "évolution",
            "number": "1",
            "interpretation": "le début, le principe masculin (yang), l’action, l’autorité."
        },
        {
            "category": "évolution",
            "number": "2",
            "interpretation": "le principe féminin (yin), le réceptif, les alliances et la sensibilité."
        },
        {
            "category": "évolution",
            "number": "3",
            "interpretation": "la création, l’enfant dans le couple, la communication."
        },
        {
            "category": "évolution",
            "number": "4",
            "interpretation": "le travail, la stabilité, la rigueur."
        },
        {
            "category": "évolution",
            "number": "5",
            "interpretation": "la liberté, le changement."
        },
        {
            "category": "évolution",
            "number": "6",
            "interpretation": "la beauté, la recherche d’équilibre et d'harmonie."
        },
        {
            "category": "évolution",
            "number": "7",
            "interpretation": "le mental, la réflexion, les études, le spirituel."
        },
        {
            "category": "évolution",
            "number": "8",
            "interpretation": "l'aspect matériel, la construction et la destruction, les gains et les pertes."
        },
        {
            "category": "évolution",
            "number": "9",
            "interpretation": "l'altruisme, la fin et le recommencement."
        },
        {
            "category": "chemin de vie",
            "number": "1",
            "interpretation": "Prise de risque, sens des responsabilités et besoin existentiel de s’élever. C’est le portrait de ceux qui poursuivent le chemin de vie numéro un. Pour eux, tout obstacle est par définition franchissable. Le numéro 1 s’individualise par l’action et mobilise sa détermination pour atteindre son objectif de vie. L’abandon et la renonciation ne font pas partie de son modus operandi, mais son surplus d’énergie peut lui jouer des tours lorsqu’il n’est pas suffisamment exploité. Frustré, il est la proie idéale de la dépression, des idées noires et des troubles de l’humeur. Le chemin de vie 1 implique la différence et l’unicité : assumé, il mène vers le succès. Non-assumé, il engendre des manifestations physiques ou psychologiques handicapantes."
        },
        {
            "category": "chemin de vie",
            "number": "2",
            "interpretation": "Le chemin de vie 2 est celui de la médiation, du travail d’équipe, du compromis et du consensus. Le 2 est empathique et perçoit le meilleur en chacun. C’est donc un excellent ami et un confident sincère. Bien que sa destinée soit globalement un long fleuve tranquille, le 2 peut accuser les contretemps de son hypersensibilité, de sa timidité ou encore de sa réticence à exprimer ses idées en public. Il évitera systématiquement la confrontation de peur d’être blessé ou contrarié, ce qui le pousse à faire profil bas. Cette attitude peut se traduire par un inconfort social qui peut très vite devenir un obstacle à son évolution personnelle et professionnelle."
        },
        {
            "category": "chemin de vie",
            "number": "3",
            "interpretation": "Le 3 est sans doute le numéro que l’on retrouve le plus dans la superstition, le folklore et la religion. Le chemin de vie 3 est celui de l’optimisme, de la pensée positive et de l’extraversion. Pour le 3, tout finira par s’arranger, quoi qu’il arrive. Sa soif de dépaysement et son appétit pour les virées exotiques l’éloignent des responsabilités, parfois même de la réalité. Pour le 3, l’art est bien plus qu’un passe-temps, c’est un mode de vie. Ce chemin de vie est aussi celui des défis, mais le numéro 3 tend à les éviter quitte à ne pas réaliser son objectif de vie. Loyal et serviable, le 3 est aussi excentrique et naïf. Il croit sincèrement au pouvoir de la conscience universelle dans la réalisation de la paix dans le monde. Le 3 fera un excellent bénévole."
        },
        {
            "category": "chemin de vie",
            "number": "4",
            "interpretation": "Solide comme un roc, droit comme un « i », le numéro 4 n’aime pas l’imprévu. La planification est une condition sine qua non à son bonheur. Il passera une partie de son existence à dresser les contours de sa zone de confort pour ne plus jamais en sortir, du moins volontairement. Parfois perçu comme trop sérieux, trop ennuyeux et trop sédentaire, celui qui poursuit le chemin de vie 4 n’est pas du genre à fanfaronner lorsqu’il rencontre le succès."
        },
        {
            "category": "chemin de vie",
            "number": "5",
            "interpretation": "Le chiffre 5 est empreint d’un symbolisme fort dans l’islam, le bouddhisme et les anciens écrits grecs. C’est le chemin de vie de l’action et de l’aventure. Le numéro 5 est une vraie dynamo qui n’arrête pas. Il va de l’avant et fait en sorte que sa vie s’éloigne de la routine, quitte à provoquer le changement sans aucune raison. C’est d’ailleurs sa principale faiblesse : il n’admet aucune frontière et conçoit l’arrêt comme un échec inacceptable. L’exploration et la soif d’apprentissage sont les seules constantes de ses « transformations » successives. De nature insatiable, celui qui poursuit le chemin de vie 5 devient exaspéré et frustré lorsqu’il n’arrive pas à satisfaire sa passion pour l’aventure."
        },
        {
            "category": "chemin de vie",
            "number": "6",
            "interpretation": "Gaïa, la déesse mère et l’ultime nourrice, représente toute la splendeur du chemin de vie 6. Ce dernier symbolise la maison, le foyer, la communauté et la compassion pour l’humain. Le 6 brille par un esprit chevaleresque rare et une bienveillance à toute épreuve qu’il mobilise pour porter secours à ceux qui en ont besoin. Porté sur le partage, il vise par cette démarche sincère à s’élever spirituellement. Si ses efforts sont largement salués par son entourage, ils sont parfois perçus comme un élan sadique qui se nourrit du malheur des autres. Son défi sera de ne pas s’oublier dans les autres."
        },
        {
            "category": "chemin de vie",
            "number": "7",
            "interpretation": "Le chemin de vie 7 est celui d’un perfectionniste solitaire qui prend conscience de son plein potentiel après un choc sentimental intense. Casanier et émotionnellement passif, il assume sa solitude et ne se soucie que peu du fait de socialiser. Il ne multiplie pas les rencontres et n’a pas besoin d’un cercle social conséquent. Philosophe, mystique, peu bavard, le 7 a un appétit pour la métaphysique et les questions existentielles qui le fascinent. La numérologie attribue au chemin de vie numéro 7 une destinée favorable et une chance supérieure à la moyenne du fait de la capacité du 7 à faire des choix rationnels appuyés par des données objectives."
        },
        {
            "category": "chemin de vie",
            "number": "8",
            "interpretation": "C’est le chemin de vie des mordus du travail. Pour atteindre la légitimité professionnelle qui catalysera sa carrière, le 8 devra prendre son mal en patience pendant plus de 10 ans. Il collectionnera les trophées et gagnera le respect de ses pairs, parfois au détriment d’une vie sociale compliquée. Les vibrations victorieuses du 8 ne sont pas spécialement liées à l’argent, mais plutôt au pouvoir. Le chemin de vie 8 ne peut s’accomplir sans un partenaire attentionné et compréhensif. Le 8 n’accepte pas les reproches qu’on lui fait à propos de son engagement professionnel."
        },
        {
            "category": "chemin de vie",
            "number": "9",
            "interpretation": "Le chemin de vie 9 est celui des voyages intérieurs, de la sensibilité artistique et de la créativité. Le 9 nourrit une passion sans limite pour tout ce qui ne lui est pas familier. S’il en trouve les moyens, il parcourra le monde pour le loisir, pour faire des affaires, pour une cause humanitaire, etc. Les 9 sont de très bon conseil du fait de l’expérience dans la vie et des kilomètres qu’ils parcourent, à la fois sur le plan géographique et spirituel."
        }
    ]
}


//Resultats : nombres utilisateur
var btn = document.getElementById('calculer');
btn.addEventListener('click', function(){

    //Réception des données du formulaire
    prenom = document.getElementById('prenom').value;
    nom = document.getElementById('nom').value;
    date = document.getElementById('dateDeNaissance').value;
    choixAnnee = document.getElementById('anneePersonnelle').value; 

    actif = nombreActif(prenom);
    hereditaire = nombreHereditaire(nom);
    expression = nombreExpression(prenom, nom);
    intime = nombreIntime(prenom, nom);
    realisation = nombreRealisation(prenom, nom);
    initial = nombreInitiales(prenom, nom);
    evolution = nombreEvolution(date);
    karma = tableKarmique(prenom, nom);
    vie = cheminDeVie(date);
    climat = climatAnnee(prenom, nom);
    
    function ChiffreActif(props){
       
        for(var i=0; i<props.post.length; i++){
            if(props.post[i].number == (actif - 1) && props.post[i].category == "actif"){
                const data = props.post[actif - 1].interpretation;

                return (
                    <div>
                        {data}
                    </div>
                )  
            }
        }
    }

    function ChiffreHereditaire(props){
       
        for(var i=0; i<props.post.length; i++){
            if(props.post[i].number == (hereditaire - 1) && props.post[i].category == "héréditaire"){
                const data = props.post[hereditaire + 8].interpretation;

                return (
                    <div>
                        {data}
                    </div>
                )  
            }
        }
    }

    function ChiffreExpression(props){
       
        for(var i=0; i<props.post.length; i++){
            if(props.post[i].number == (expression - 1) && props.post[i].category == "expression"){
                const data = props.post[expression + 17].interpretation;

                return (
                    <div>
                        {data}
                    </div>
                )  
            }
        }
    }

    function ChiffreIntime(props){
       
        for(var i=0; i<props.post.length; i++){
            if(props.post[i].number == (intime - 1) && props.post[i].category == "intime"){
                const data = props.post[intime + 26].interpretation;

                return (
                    <div>
                        {data}
                    </div>
                )  
            }
        }
    }

    function ChiffreRealisation(props){
       
        for(var i=0; i<props.post.length; i++){
            if(props.post[i].number == (realisation - 1) && props.post[i].category == "réalisation"){
                const data = props.post[realisation + 35].interpretation;

                return (
                    <div>
                        {data}
                    </div>
                )  
            }
        }
    }

    function ChiffreInitial(props){
       
        for(var i=0; i<props.post.length; i++){
            if(props.post[i].number == (initial - 1) && props.post[i].category == "issu des initiales"){
                const data = props.post[initial + 44].interpretation;

                return (
                    <div>
                        {data}
                    </div>
                )  
            }
        }
    }

    function ChiffreEvolution(props){
       
        for(var i=0; i<props.post.length; i++){
            if(props.post[i].number == (evolution - 1) && props.post[i].category == "évolution"){
                const data = props.post[evolution + 53].interpretation;

                return (
                    <div>
                        {data}
                    </div>
                )  
            }
        }
    }

    function CheminVie(props){
       
        for(var i=0; i<props.post.length; i++){
            if(props.post[i].number == (vie - 1) && props.post[i].category == "chemin de vie"){
                const data = props.post[vie + 62].interpretation;

                return (
                    <div>
                        {data}
                    </div>
                )  
            }
        }
    }
    

    function Affichage(props){

        
        return(
            <div>
                <h2>Interprétations</h2>
                <div id="chiffre">
                    <p>Votre chiffre actif est le numéro {actif}</p>
                    <p>Votre chiffre héréditaire est le numéro {hereditaire}</p>
                    <p>Votre chiffre d'expression est le numéro {expression}</p>
                    <p>Votre chiffre intime est le numéro {intime}</p>
                    <p>Votre chiffre de réalisation est le numéro {realisation}</p>
                    <p>Votre chiffre issu de vos initiales est le numéro {initial}</p>
                    <p class="table">Votre chiffre d'évolution est le numéro {evolution}</p>
                    <p>Table karmique :</p>
                    <p class="table">{karma}</p>
                    <p>Votre chemin de vie est le numéro {vie}</p>
                    <p class="table">{climat}</p>
                </div>
                <h3>Chiffre actif n° {actif}</h3>
                <ChiffreActif post={data.results}/>
                <h3>Chiffre héréditaire n° {hereditaire}</h3>
                <ChiffreHereditaire post={data.results}/>
                <h3>Chiffre d'expression n° {expression}</h3>
                <ChiffreExpression post={data.results}/>
                <h3>Chiffre inttime n° {intime}</h3>
                <ChiffreIntime post={data.results}/>
                <h3>Chiffre de réalisation n° {realisation}</h3>
                <ChiffreRealisation post={data.results}/>
                <h3>Chiffre issu des initiales n° {initial}</h3>
                <ChiffreInitial post={data.results}/>
                <h3>Chiffre d'évolution n° {evolution}</h3>
                <ChiffreEvolution post={data.results}/>
                <h3>Aide à la compréhension de votre table karmique</h3>
                <div id="explication">
                    <p>Le Karma est une discipline dérivée de la numérologie qui représente une succession d’épreuves que l’on s’est choisies juste avant sa naissance, et que l'on doit surmonter tout au long de notre vie. Le Karma est caractérisé par des nombre karmiques, présentés dans un tableau d’inclusion, appelé aussi table karmique. Les cases qui ont un 0, sont celles qui correspondent aux Karmas. Les chiffres karmiques, sont aussi appelés chiffres manquants et correspondent aux leçons à apprendre de votre vie. Chaque chiffre karmique a une interprétation spécifique que vous trouverez ci-dessous. Vous n'avez plus qu'à travailler sur les caractères spécifiques manquants pour améliorer votre comportement. </p>
                    <p>Le karma 1 invite à travailler sur l’égoïsme et la domination, ou encore, sur le manque de volonté et d’assurance.</p>
                    <p>Le karma 2 invite à travailler sur la dépendance et la soumission ou sur l’impatience et la difficulté à s’associer avec autrui.</p>
                    <p>Le karma 3 invite à travailler quant à lui, sur l’enfermement ou encore sur l’orgueil, le mensonge et la dispersion.</p>
                    <p>Le karma 4 invite à travailler sur la paresse et le manque d’organisation, ou bien sur la rigidité et l’étroitesse d’esprit.</p>
                    <p>Le karma 5 invite à travailler soit sur la rigidité face à la nouveauté ou sur l’impulsivité et l’instabilité.</p>
                    <p>Le karma 6 invite à travailler sur l’intolérance et l’irresponsabilité, ou bien sur l’amour possessif et la complaisance.</p>
                    <p>Le karma 7 invite à travailler quant à lui, sur le fanatisme et le masochisme, ou encore, sur le manque de foi et de compréhension.</p>
                    <p>Le karma 8 invite à travailler soit sur l’abus du pouvoir et l’intérêt excessif pour le matériel, soit sur le refus du pouvoir et le manque de maîtrise de soi.</p>
                    <p>Le karma 9 invite à travailler sur l’égoïsme et le manque d’intérêt pour les autres, ou bien, sur le dévouement excessif pour autrui.</p>
                </div>
                <h3>Chemin de vie n° {vie}</h3>
                <CheminVie post={data.results}/>
            </div>
        )   
        
    }

    ReactDom.render(	
        <Affichage post={data.results} />,
        document.getElementById('app')
    )

});